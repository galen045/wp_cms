<?php
header("Content-Type:application/json");
include_once ("dbconnect.php");

if(!empty($_GET['film']) && !empty($_GET['year'])) // Film and Year included
{
	$film=$_GET['film'];
	$year=$_GET['year'];
    $items = getFilmsByNameAndYear($film, $year, $conn);
	
	if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else if(!empty($_GET['year']))  // Year included
{
    $year=$_GET['year'];
    $items = getFilmsByYear($year, $conn);
	
    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else if(!empty($_GET['film'])) // Film included
{
    $film=$_GET['film'];
    $items = getFilmsByName($film, $conn);
	
    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else // None included
{
    $items = getAllFilms($conn);

    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}


/*
if(!empty($_GET['year']) && empty($_GET['film']))  // Year included
{
    $year=$_GET['year'];
    $items = getFilmsByYear($year, $conn);
	
    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
} 
else if(!empty($_GET['film']) && empty($_GET['year'])) // Film included
{
	$film=$_GET['film'];
    $items = getFilmsByName($film, $conn);
	
    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else if(!empty($_GET['film']) && !empty($_GET['year'])) // Film and Year included
{
	$film=$_GET['film'];
	$year=$_GET['year'];
    $items = getFilmsByNameAndYear($film, $year, $conn);
	
	if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else if(empty($_GET['film']) && empty($_GET['year'])) // None included
{
    $items = getAllFilms($conn);
	echo $items;
    if (empty($items))
    {
        jsonResponse(200, "No films found", NULL);
    }
    else
    {
        jsonResponse(200, "Film/S found", $items);
    }
}
else 
{  echo"test";
	jsonResponse(400,"Invalid Request",NULL);
}
*/
function jsonResponse($status, $status_message, $data)
{
	header("HTTP/1.1 " . $status_message);
	$response['status'] = $status;
	$response['status_message'] = $status_message;
	$response['data'] = $data;
	$json_response = json_encode($response);
	echo $json_response;
}

function getAllFilms($conn)
{    
    $sql = "SELECT * FROM api_films";
    
	$resultset = mysqli_query($conn, $sql) or die("database error:" . mysqli_error($conn));
	$data = array();
	
	while ($rows = mysqli_fetch_assoc($resultset))
	{
		$data[] = $rows;
	}

	return $data;
}

function getFilmsByYear($year, $conn)
{    
    $sql = "SELECT * FROM api_films WHERE Year >= ".$year;
    
	$resultset = mysqli_query($conn, $sql) or die("database error:" . mysqli_error($conn));
	$data = array();
	
	while ($rows = mysqli_fetch_assoc($resultset))
	{
		$data[] = $rows;
	}

	return $data;
}

function getFilmsByName($film, $conn)
{    
    $sql = "SELECT * FROM api_films WHERE Name LIKE '%".$film."%'";
    
	$resultset = mysqli_query($conn, $sql) or die("database error:" . mysqli_error($conn));
	$data = array();
	while ($rows = mysqli_fetch_assoc($resultset))
	{
		$data[] = $rows;
	}

	return $data;
}

function getFilmsByNameAndYear($film, $year, $conn)
{    
    $sql = "SELECT * FROM api_films WHERE Name LIKE '%".$film."%' AND Year >= ".$year;
    
	$resultset = mysqli_query($conn, $sql) or die("database error:" . mysqli_error($conn));
	$data = array();
	while ($rows = mysqli_fetch_assoc($resultset))
	{
		$data[] = $rows;
	}

	return $data;
}

?>
