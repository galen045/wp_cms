<?php 

/* --- Short codes ------------------------------------- */

/* 1. Shop location (Google Maps) */

function shop_location_map()
{
	return '<div id="map"></div>';
}

add_shortcode('shop_location_map_shortcode', 'shop_location_map');

/* 2. Countdown to Sale */

function countdown_sale()
{
	return "<div class='sc_countdown_section'>
				<h2 class='countdown_header'> 
					Upcoming Sale!
				</h2>			
				<h4 class='countdown_text_top'>
					All comic books reaching up to 50% off
				</h4> 		
				<p class='countdown' id='countdown'></p>
			</div>
			";
}

add_shortcode('countdown_film_shortcode', 'countdown_sale');

/* 3. FAQ */

function faq(){
	return "<h3 class='faq_title'> FAQ </h3>			
			<section>
				<article>
					 <details>
						<summary class='faq-card-header'> How long will the items take to arrive? </summary>						
						<div class='faq-card-body'>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
							magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
							consequat.
						</div>									
					</details>
					<details>
						<summary class='faq-card-header'> Can I return the items after a week? </summary>						
						<div class='faq-card-body'>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
							magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
							consequat.
						</div>
					</details>
					<details>
						<summary class='faq-card-header'> Can I send items as a gift? </summary>						
						<div class='faq-card-body'>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore 
							magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
							consequat.
						</div>
					</details>					
				</article>
			</section";
}

add_shortcode('faq_shortcode', 'faq');	

/* 4. Latest posts */

function latest_post($atts)
{
	$args = array
	(
		'showposts' => $atts['showposts'],
		'post_type' => $atts['post_type'],
		'orderby' 	=> $atts['orderby'],
		'order'  	=> $atts['order'],
	);		
	
	query_posts( $args );
	
	if (have_posts()) :
		while (have_posts()) : the_post();			
			$data .= 	'<div class="latest_post_sc">
							<a class="latest_post_sc_anounce" href="' .get_the_permalink(). '"> View article</a>
							<h3 class="latest_post_sc_title">'.get_the_title().'</h3>
							<p class="latest_post_sc_content">'.get_the_excerpt().'</p>
						</div>';
						
		endwhile;
		
		wp_reset_query();
		
	endif;
	
	return $data; 		
}

add_shortcode('latest_posts_shortcode', 'latest_post');	

/* 5. Latest products */

function latest_prod($atts)
{	
	$args = array
	(
		'post_type'      	=> $atts['post_type'],
        'posts_per_page' 	=> $atts['posts_per_page'],       
		'order' 			=> $atts['order'],
    );

	$loop = new WP_Query( $args );
	$res = '';
	
    while ( $loop->have_posts() ) : $loop->the_post();
        global $product;
			$res .= '<div class="latest_prod_card">   											
						<a href="'.get_permalink().'">' . woocommerce_get_product_thumbnail().'</a>
					</div>';
	
        
    endwhile;
	wp_reset_query();
	
	return '<div class="latest_prod_container">'.$res.'</div>';
}

add_shortcode('latest_products_shortcode', 'latest_prod');

/* 6. About us */

function about_us(){
	return '<div class="sc_about_section"> 
				<h3 class="sc_about_title"> About us. </h3>
				<p class="sc_about_text"> 
					Quisque blandit dolor risus, sed dapibus dui facilisis sed. Donec eu porta elit. 
					Aliquam porta sollicitudin ante, ac fermentum orci mattis et. Phasellus ac nibh 
					eleifend, sagittis purus nec, elementum massa. Quisque tincidunt sapien a sem 
					porttitor, id convallis dolor pharetra. Donec tempor cursus facilisis. 
				</p>
				<div class="covid_disclaimer">
					<p class="covid_title"> COVID-19 situation </p>
					<p class="covid_text"> 
						By virtue of this Legal Notice, the government orders the closure of further 
						business establishments to which the public is admitted. Specifically, the 
						Superintendent of Public Health orders the closure of all shops whereby their 
						principal business relates to selling of clothing, sportswear, jewellery, hand 
						bags and leather goods, costume  jewellery  and  accessories,  footwear,  
						non-prescription  eye-wear, perfumeries, beauty products, haberdasheries, 
						soft furnishings, souvenir  shops,  discount  stores,  luggage  shops,  toy  
						shops,  hobby shops, furniture shops, florists and vaping shops, whether operating 
						in shopping malls and/or elsewhere.
						<br>
						<br>
						However, these shops may still provide delivery services of their products to the community. </p>
				</div>
			</div>';
}

add_shortcode('about_us_shortcode', 'about_us');

/* 7. Shop opening INFO */

function opening_info(){
	return '<div class="sc_opening_info_section"> 
				<h3 class="sc_opening_info_title"> Opening times. </h3>
				<div class="seperator"> </div>
				<div class="opening_info_days_section">
					<p class="sc_opening_info_weekday">Monday - Friday</p>
					<p class="sc_opening_info_weekday_time"> 09:00 - 19:00 </p>
					<p class="sc_opening_info_weekday">Saturday - Sunday</p>
					<p class="sc_opening_info_weekday_time"> 10:00 - 21:00 </p>
				</div>
				<div class="seperator"> </div>
				<a href="contact" class="sc_opening_info_section_contact"> Contact </a>				
			</div>';
}

add_shortcode('opening_info_shortcode', 'opening_info');			

/* API */	

function api_search_form() 
{
    echo '<form action="'.esc_url($_SERVER['REQUEST_URI']).'" method="POST">';

	echo '<p> Search by year </p>';
    echo '<input type="text" class="api_search" name="search_year" value="' . ( isset( $_POST["search_year"] ) ? esc_attr( $_POST["search_year"] ) : '' ) . '" size="40" />';	

	echo '<p> Search by name </p>';	
	echo '<input type="text" class="api_search" name="search_film" value="' . ( isset( $_POST["search_film"] ) ? esc_attr( $_POST["search_film"] ) : '' ) . '" size="40" />';	
	
    echo '<p><input type="submit" name="search-films" value="Search"/></p>';
    echo '</form>';
}

function api_search_functionality() 
{
    // if the submit button is clicked, send the email
    if ( isset( $_POST['search-films'] ) ) 
	{
		$year = $_POST['search_year'];
		$name = $_POST['search_film'];
		
		if($year != "" && $name == "")
		{
			echo   '[jsoncontentimporter url="http://localhost:8080/wp_cms/wp-content/themes/twentytwenty-childtheme/read.php?year='.$year.'" basenode="data"]
						<div class="rows">
							<div class="col-md-6">
								<div class="top-rated-movies"> 
									<h5>{name}</h5>
									Year: {year}<br>
									Rating: {rating}<br>
									URL: <a href="{URL}"> Link to IMDB site </a>
								</div>
							</div>
						</div>
					[/jsoncontentimporter]';
		}				
		else if($year == "" && $name != "")
		{
			echo   '[jsoncontentimporter url="http://localhost:8080/wp_cms/wp-content/themes/twentytwenty-childtheme/read.php?film='.$name.'" basenode="data"]
						<div class="rows">
							<div class="col-md-6">
								<div class="top-rated-movies"> 
									<h5>{name}</h5>
									Year: {year}<br>
									Rating: {rating}<br>
									URL: <a href="{URL}"> Link to IMDB site </a>
								</div>
							</div>
						</div>
					[/jsoncontentimporter]';
		}
		else if($year != "" && $name != "")
		{
			echo   '[jsoncontentimporter url="http://localhost:8080/wp_cms/wp-content/themes/twentytwenty-childtheme/read.php?film='.$name.'&year='.$year.'" basenode="data"]
						<div class="rows">
							<div class="col-md-6">
								<div class="top-rated-movies"> 
									<h5>{name}</h5>
									Year: {year}<br>
									Rating: {rating}<br>
									URL: <a href="{URL}"> Link to IMDB site </a>
								</div>
							</div>
						</div>
					[/jsoncontentimporter]';
		}
		else if($year == "" && $name == "")
		{
				echo   '[jsoncontentimporter url="http://localhost:8080/wp_cms/wp-content/themes/twentytwenty-childtheme/read.php" basenode="data"]
						<div class="rows">
							<div class="col-md-6">
								<div class="top-rated-movies"> 
									<h5>{name}</h5>
									Year: {year}<br>
									Rating: {rating}<br>
									URL: <a href="{URL}"> Link to IMDB site </a>
								</div>
							</div>
						</div>
					[/jsoncontentimporter]';
		}
    }
}

function api_shortcode() 
{   
	ob_start();
    api_search_form();
	api_search_functionality();   
    return ob_get_clean();
}

add_shortcode( 'search_form', 'api_shortcode' );

?>





















